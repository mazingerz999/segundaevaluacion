<?php


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function validarNombre($nombre) {
    if (strlen($nombre)>3) {
        return true;
    }else{
        return false;
    }
}

function dni($dni){
  $letra = substr($dni, -1);
  $numeros = substr($dni, 0, -1);
  if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
      return true;
  }else{
    return false;
  }
}

function validarPassword($pass1, $pass2) {
    if ($pass1==$pass2) {
        return true;
    }else{
        return false;
    }
}

    $datos= [];
    
    if (!validarNombre($_POST["nombre"])) {
        $datos["errorNombre"]= utf8_encode("El nombre debe tener mas letras");
    }
    if (!dni($_POST["dni"])) {
        $datos["errorDNI"]= utf8_encode("El DNI es incorrecto");
    }
    if (!validarPassword($_POST["password1"], $_POST["password2"])) {
        $datos["errorPassword"]= utf8_encode("El password es incorrecto");
    }

echo json_encode($datos);