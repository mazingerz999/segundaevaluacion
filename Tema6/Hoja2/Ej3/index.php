<?php
require_once('Medico.php');
require_once('Familia.php');
require_once('Urgencia.php');


$fam1=new Familia("Alberto", 31, "Mañana", 15);
$fam2=new Familia("Jose", 21, "Tarde", 20);
$fam3=new Familia("Carlos", 67, "Tarde", 5);
$urg1=new Urgencia("Alberto", 81, "Tarde", "Cuidados Intensivos");
$urg2=new Urgencia("Jose", 21, "Tarde", "Cirugia");
$urg3=new Urgencia("Carlos", 27, "Tarde", "Cirugia");

$arrayMedicos=[
    "Familia"=>[$fam1,$fam2,$fam3],
    "Urgencia"=>[$urg1,$urg2,$urg3]
];
echo "<h3>Lista de todos los medicos</h3>";
foreach ($arrayMedicos as $clave => $valor){
    foreach ($valor as $item){
        echo $item."<br>";
    }
}

echo "<h3>Medicos de Tarde y Mayores de 60</h3>";
foreach ($arrayMedicos as $clave => $valor){
    foreach ($valor as $item){
        if (($item->getTurno()=="Tarde") && ($item->getEdad()>60)){
            echo $item."<br>";
        }

    }
}
echo "<h3>Medicos con mayor numero de pacientes</h3>";
if (isset($_POST['enviar'])){


    foreach ($arrayMedicos as $clave => $valor){
        foreach ($valor as $medico){
            if ($medico instanceof Familia && $medico->getNumPacientes() >= $_POST["num"]){
                echo $medico."<br>";
            }

        }
    }


}

?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Document</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' rel='stylesheet'
          integrity='sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6' crossorigin='anonymous'>
</head>
<body>
<form action="" method="post">
    <p><label for="num">Introduce el numero de pacientes</label><input type="number" name="num" id="num"></p>
    <input type="submit" value="Enviar" name="enviar" id="enviar">
</form>
</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
        integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
        crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js'
        integrity='sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG' crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js'
        integrity='sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc' crossorigin='anonymous'></script>
</html>