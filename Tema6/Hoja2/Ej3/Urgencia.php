<?php


class Urgencia extends Medico
{

    private $unidad;


    public function __construct($nombre, $edad, $turno, $unidad)
    {
        parent::__construct($nombre, $edad, $turno);
        $this->unidad = $unidad;
    }

    /**
     * @return mixed
     */
    public function getUnidad()
    {
        return $this->unidad;
    }

    /**
     * @param mixed $unidad
     */
    public function setUnidad($unidad): void
    {
        $this->unidad = $unidad;
    }


    public function __toString()
    {
        return parent::__toString(). " Unidad: ".$this->getUnidad();
    }


}