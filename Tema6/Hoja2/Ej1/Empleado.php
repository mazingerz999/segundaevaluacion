<?php 

class Empleado {
  
    private $sueldo;
    function __construct($sueldo) {
        $this->sueldo = $sueldo;
    }
    function getSueldo() {
        return $this->sueldo;
    }

    function setSueldo($sueldo): void {
        $this->sueldo = $sueldo;
    }

    public function __toString()
    {
      return "El dinero disponible es de: ".$this->getSueldo();  
    }
}
