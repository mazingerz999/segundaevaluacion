<?php

require_once( 'Empleado.php' );

class Encargado extends Empleado{

    function __construct($sueldo) {
        
        parent::__construct($sueldo);
    }
    function getSueldo() {
        return parent::getSueldo()*1.15;
    }

 
}