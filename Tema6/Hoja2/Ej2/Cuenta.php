<?php 

class Cuenta {
  
    protected $nombre;
    protected $titular;
    protected $saldo;

    function __construct($nombre, $titular, $saldo) {
        $this->nombre = $nombre;
        $this->titular = $titular;
        $this->saldo = $saldo;
    }
 


    

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of titular
     */ 
    public function getTitular()
    {
        return $this->titular;
    }

    /**
     * Set the value of titular
     *
     * @return  self
     */ 
    public function setTitular($titular)
    {
        $this->titular = $titular;

        return $this;
    }

    /**
     * Get the value of saldo
     */ 
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * Set the value of saldo
     *
     * @return  self
     */ 
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;

        return $this;
    }

    public function ingreso($saldo2)
    {
       $this->saldo+=$saldo2;
    }
    public function reintegro($saldo2)
    {
       $this->saldo-=$saldo2;
    }
    public function esPreferencial($aux)
    {
        if ($this->saldo>$aux) {
            return true;
        }else{
            return false;
        }
       
    }

    public function __toString()
    {
      return "Nombre: ".$this->getNombre(). " Titular: ".$this->getTitular(). " Saldo: ".$this->getSaldo();
    }

}