<?php

class Circulo{
  
    private $radio;

    public function __construct($radio)
    {
        $this->radio = $radio;
    }

    /**
     * Get the value of radio
     */ 
    public function getRadio()
    {
        return $this->radio;
    }

    /**
     * Set the value of radio
     *
     * @return  self
     */ 
    public function setRadio($radio)
    {
        $this->radio = $radio;

        return $this;
    }
    //////////////////METODOS MAGICOS////////////////////////
    public function __set($name, $value){
        if(property_exists($this, $name)) {
            $this->$name = $value;
        } 
    }
    public function __get($name){
        if(property_exists($this, $name)) {
            return $this->$name;
        }
    }
    public function __toString()
    {
      return $this->radio;  
    }
    //////////////////////////////////////////////////
    
}
