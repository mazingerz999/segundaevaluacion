<?php

namespace Monedero;

class Monedero
{
    private $dinero;
   static private $monederos;

    public function __construct($dinero)
    {
        $this->dinero = $dinero;
        self::$monederos++;
        
    }

    public function meterDinero($aux)
    {
        # code...
        $this->dinero+=$aux;
        
    }
    public function sacarDinero($aux)
    {
        # code...
        
        if ($this->dinero-$aux<$this->dinero) {
            # code...
            $this->dinero=$this->dinero;
        }else{
            $this->dinero-=$aux;
        }

    }
    public function __toString()
    {
      return "El dinero disponible es de $this->dinero";  
    }

    static function aumentaMonederos() {
        self::$monederos++;
   }
   static function restaMonederos() {
        self::$monederos--;
   }

   function setDinero($dinero): void {
       $this->dinero = $dinero;
   }

   static function setMonederos($monederos): void {
       self::$monederos = $monederos;
   }
   static function getMonederos() {
    return self::$monederos;
    }

}
