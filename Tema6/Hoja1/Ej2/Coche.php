<?php

class Coche{
   
    private $matricula;
    private $velocidad;


    public function __construct($matricula, $velocidad){
        $this->matricula = $matricula;
        $this->velocidad = $velocidad;
    }
    

     /**
      * Get the value of matricula
      */ 
     public function getMatricula()
     {
          return $this->matricula;
     }

     /**
      * Set the value of matricula
      *
      * @return  self
      */ 
     public function setMatricula($matricula)
     {
          $this->matricula = $matricula;

          return $this;
     }

    /**
     * Get the value of velocidad
     */ 
    public function getVelocidad()
    {
        return $this->velocidad;
    }

    /**
     * Set the value of velocidad
     *
     * @return  self
     */ 
    public function setVelocidad($velocidad)
    {
        $this->velocidad = $velocidad;

        return $this;
    }
    public function __toString()
    {
        return "Velocidad: ".$this->velocidad."Matricula: ". $this->matricula;
    }

    public function acelerar($aux)
    {
        
        if ($this->velocidad+$aux>120 ) {
            # code...
            $this->velocidad=120;

        }else{
            $this->velocidad+=$aux;
        }
    }
    public function frenar($aux)
    {
        # code...
        if ($this->velocidad-$aux<0 ) {
            # code...
            $this->velocidad=0;

        }else{
            $this->velocidad-=$aux;
        }
    }

}
