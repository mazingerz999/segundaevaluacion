<?php
$animales = array(
    'especie' => 'Bisontedfncvnvncvn',
    'peso' => 1000,
    'altura' => 190,
    'fechaNacimiento' => '2014-09-07',
    'imagen' => 'bisonte.jpg',
    'alimentacion' => 'herbívoro',
    'descripcion' => 'Los bisontes son potentes ungulados de carácter muy vivaz y de una asombrosa agilidad. Forman rebaños independientes de machos y hembras que se unen únicamente en la época de celo, en la que los machos están más gordos y vigorosos. Finalizado este periodo, las hembras se alejan de la manada y vuelven a su vida tranquila. Nueve meses después dan a luz un ternero, que permanecerá junto a su madre.
			El carácter del bisonte se modifica con el paso de los años: de joven es alegre y vivaz y, aunque no es especialmente manso, tampoco se muestran agresivos. Al hacerse viejos, sobre todo los machos, se vuelven rudos y se irritan fácilmente, llegando a ser peligrosos.'
);

$url_servicio = "http://zoologico.laravel/rest/insertar";
$curl = curl_init($url_servicio);

curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($curl, CURLOPT_POSTFIELDS, $animales);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$respuesta_curl = curl_exec($curl);
curl_close($curl);
$respuesta_decodificada = json_decode($respuesta_curl);

var_dump($respuesta_decodificada);



